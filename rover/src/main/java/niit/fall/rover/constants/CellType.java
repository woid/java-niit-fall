package niit.fall.rover.constants;

public enum CellType {
    DOWNHILL, //спуск под гору
    UPHILL, //подъем на гору
    PLANE //плоскость
}
