package niit.fall.rover;

/**
 * Это исключение должно быть проверяемым - оно возникает, если ровер вышел за пределы поверхности
 */
public class OutOfGroundException {
}
